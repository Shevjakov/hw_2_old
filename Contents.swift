import UIKit

//import Foundation

 /*
 Task 1
 
 Create a variable named counter and set it equal to 0.
 Create a while loop with the condition counter < 10 which prints out counter is X (where X is replaced with counter value) and then increments counter by 1.
 */

var counter1 = 0
while counter1 < 10 {
    counter1+=1
print(counter1)
}


 /*
 Task 2
 
 Create a variable named counter and set it equal to 0.
 Create another variable named roll and set it equal to 0.
 Create a repeat-while loop.
 Inside the loop, set roll equal to Int.random(in: 0...5) which means to pick a random number between 0 and 5.
 Then increment counter by 1.
 Finally, print After X rolls, roll is Y where X is the value of counter and Y is the value of roll.
 Set the loop condition such that the loop finishes when the first 0 is rolled.
 */

var counter = 0

var roll = 0

repeat {
    roll = Int.random(in: 0...5)
    counter+=1
}while roll > 0; do {
    print("After", counter, "rolls")
}


/*
 Task 3
 
 Create a constant named range, and set it equal to a range starting at 1 and ending with 10 inclusive.
 Write a for loop that iterates over this range and prints the square of each number.
 */

let range = 1...10
for range in 1...10 {
    print(range * range)
    
}


/*
 Task 4
 
 Below you can see an example of for loop that iterates over only the even rows like so:
 */

//var sum = 0
//for row in 0..<8 {
//    if row % 2 == 0 {
//        continue
//    }
//    for column in 0..<8 {
//        sum += row * column
//    }
//}
//print(sum)

var sum = 0
for row in 0..<8 where (row % 2 == 0){
        for column in 0..<8 {
            sum += row * column
        }
}
print(sum)


/*
 Change this to use a where clause on the first for loop to skip even rows instead of using continue. Check that the sum is 448 as in the initial example.
 */



/*
 Task 5
 
 Print a table of the first 10 powers of 2
 */
var tableOf = 2
for index in 1...10 {
    print("\(tableOf) X \(index) = \(index * tableOf)")
}


/*
 Task 6
 
 Given a number n, calculate the factorial of n.
 Example: 4 factorial is equal to 1 * 2 * 3 * 4
 */

var number: Int = 4 // Change the number here

var factorial: Int = 1

var n: Int = number + 1

for i in 1..<n{
    
    factorial = factorial * i
    
}

print("Factorial of ",number," is: ", factorial)


/*
 Task 7
 
 Given a number n, calculate the n-th Fibonacci number.
 (Recall Fibonacci is 1, 1, 2, 3, 5, 8, 13, ...
 Start with 1 and 1 and add these values together to get the next value.
 The next value is the sum of the previous two.
 So the next value in this case is 8+13 = 21.)
 */

// returns an array containing the first n Fibonacci numbers
func fibonacci(n: Int) -> [Int] {
    
    assert(n > 1)
    
    var array = [0, 1]
    
    while array.count < n {
        
        array.append(array[array.count - 1] + array[array.count - 2])
    }
    return array
}

fibonacci(n: 10) // [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]

/*
 Task 8
 
 Write a switch statement that takes an age as an integer and prints out the life stage related to that age. You can make up the life stages, or use my categorization as follows: 0-2 years, Infant; 3-12 years, Child; 13-19 years, Teenager; 20-39, Adult; 40-60, Middle aged; 61+, Elderly.
 */



/*
 Task 9
 
 Write a switch statement that takes a tuple containing a string and an integer. The string is a name, and the integer is an age. Use the same cases that you used in the previous exercise and let syntax to print out the name followed by the life stage. For example, for myself it would print out "Slava is an adult."
 */



/*
 Task 10
 
 Create a constant called myAge and set it to your age. Then, create a constant named isTeenager that uses Boolean logic to determine if the age denotes someone in the age range of 13 to 19.
 */



/*
 Task 11
 Create another constant named theirAge and set it to my age, which is 30. Then, create a constant named bothTeenagers that uses Boolean logic to determine if both you and I are teenagers.
 */



/*
 Task 12
 Create a constant named reader and set it to your name as a string. Create a constant named author and set it to my name, Matt Galloway. Create a constant named authorIsReader that uses string equality to determine if reader and author are equal.
 */



/*
 Task 13
 
 Создайте массив "дни в месяцах":
 Распечатайте элементы, содержащие количество дней в соответствующем месяце, используя цикл for и этот массив.
 */



/*
 Task 14
 Создать в if и отдельно в switch программу которая будет смотреть на возраст человека и говорить куда ему идти в школу, в садик, в универ, на работу или на пенсию и тд.
 */



/*
 Task 15
 В switch и отдельно в if создать систему оценивания школьников по 12 бальной системе и высказывать через print мнение.
 */

